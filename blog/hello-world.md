# Hello World!
Welcome to my blog! I finally started it. I wanted to do it for some time, but the facts that:
 * I wanted to use WordPress, or any other CMS,
 * I don't have any domain

were stopping me from doing it. But today I thought - hey, who said I need to host it on a CMS with rich backend side? I can host everything as static HTML/CSS and possibly JS, maintain RSS feed myself by just editing one file and then maybe push it to GitLab Pages (even if I have a VPS draining 14 euro quarterly).

Genius idea - I thought, and so I started writing this post. In Markdown. My first idea was to store the files in a directory and then make user read the files using some simple JS stuff, along with changing Markdown to HTML at fly, but... well it was too complicated and for somebody with disabled JavaScript in browser the website wouldn't work. And there also comes compatiblity with as many browsers as possible. Even if I think that nowadays using IE6, or just any IE version is just really stupid, but... It'd be nice to support everything, including text-mode browsers. :)

So, here I go, with maintaining everything myself and hosting it on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) (because fortunately GitLab actually has this feature!).

---
*08.06.2018*